'use strict'

const express = require('express')
const mongoClient = require('mongodb').MongoClient
const ObjectId = require("mongodb").ObjectId
const bodyParser = require('body-parser')
const bcrypt = require('bcryptjs')
const crypto = require('crypto')

// Constants
const DB_URL = `mongodb://mongo-db:27017/TimerDb`
const PORT = 8102
const TOKEN_VALIDITY = 60 * 60 * 24 * 30 // one month

// App
const app = express()
const saltLevel = 4
const tokenLength = 33

app.use(bodyParser.json())
var cors = require('cors')
app.use(cors())

// TODO: time of update
app.get('/', (req, res) => {
    res.send('Hello world!\n')
});

function isEmpty(str) {
    return !str || str.length === 0 
}

function getEpoch() {
    return Math.round((new Date()).getTime() / 1000)
}

class ServerError extends Error {
    constructor(code, message) {
        super(message)
        this.code = code
    }
}

class UnauthorizedError extends ServerError {
    constructor() {
        super(401, 'Unauthorized!')
    }
}

class DatabaseError extends ServerError {
    constructor() {
        super(500, 'Database error!')
    }
}


function generateToken() {
    return new Promise((resolve, reject) => {
        crypto.randomBytes(tokenLength, (err, buf) => {
            if (err) {
                reject(err)
            }
            resolve(buf.toString('base64'))
        })
    })
}

async function createTokens(id) {
    let token = await generateToken();
    return {
        userId: id,
        tokenExpiration: getEpoch() + TOKEN_VALIDITY,
        token
    }
}

const asyncMiddleware = fn =>
  (req, res, next) => {
    fn(req, res, next).catch(next);
  };

app.options('/login', (req, res) => {
    console.log("XXXXXX")
})
app.post('/login', asyncMiddleware(async (req, res, next) => {
    let login = req.body.login
    if(isEmpty(login)) throw new ServerError(400, "Missing `login` field")
    let db = await mongoClient.connect(DB_URL)
    let foundUser = await db.collection('users').findOne({login})
    if(!foundUser) throw new ServerError(404, "User not found")
    let passwordMatch = await bcrypt.compare((req.body.password || ""), foundUser.password)
    if (!passwordMatch) throw new ServerError(401, "Invalid credentials!")
    let tokens = await createTokens(foundUser._id)
    await db.collection('tokens').findOneAndReplace({userId: tokens.userId}, tokens, {upsert: true})
    db.close();
    return res.json(tokens)
}));

async function verifyAuthorized(headers, db) {
    let token = headers.authorization || ""
    if(token.includes("Bearer ")) {
        token = token.substring(7);
    }
    let foundToken = await db.collection('tokens').findOne({token})
    if(!foundToken) throw new UnauthorizedError()
    return foundToken.userId
}

app.post('/timer', asyncMiddleware(async (req, res, next) => {
    let db = await mongoClient.connect(DB_URL)
    let userId = await verifyAuthorized(req.headers, db)
    if(isEmpty(req.body.id)) throw new ServerError(400, "Missing `id` field")
    let id = ObjectId(req.body.id)
    let existingTimer = await db.collection('timers').findOne({_id: id, userId})
    if(!existingTimer) throw new ServerError(404, "Timer not found!")
    if(!req.body.endTime) throw new ServerError(400, "Missing `endTime` field")
    let endTime = req.body.endTime || 0
    if(endTime < 1) throw new ServerError(202, "`endTime` should be non-zero value!")
    if(endTime <= existingTimer.startTime) throw new ServerError(202, "`endTime` must be after `starTime`!")
    let tagId = req.body.tagId ? ObjectId(req.body.tagId) : existingTimer.tagId
    let tag = await db.collection('tags').findOne({_id: tagId})
    if(!tag) throw new ServerError(404, 'Specified tag do not exists!')
    let replaceResult = await db.collection('timers').findOneAndReplace({_id: id, userId}, 
        {startTime: existingTimer.startTime, endTime, tagId, userId, _id: id})
    if(replaceResult.ok !== 1) throw new DatabaseError()
    db.close()
    res.json({tagId, startTime: existingTimer.startTime, endTime, id})
}));

app.put('/timer', asyncMiddleware(async (req, res, next) => {
    let db = await mongoClient.connect(DB_URL)
    let userId = await verifyAuthorized(req.headers, db)
    let startTime = req.body.startTime
    if(isEmpty(req.body.tagId)) throw new ServerError(400, "Missing `tagId` field")
    if(!startTime) throw new ServerError(400, "Missing `startTime` field")
    if((startTime || 0) < 1) throw new ServerError(202, "`startTime` should be non-zero value!")
    let tagId = ObjectId(req.body.tagId)
    let tag = await db.collection('tags').findOne({_id: tagId})
    if(!tag) throw new ServerError(404, 'Specified tag do not exists!')
    let insertResult = await db.collection('timers').insertOne({tagId, userId, startTime})
    if(insertResult.insertedCount !== 1) throw new DatabaseError()
    db.close()
    res.json({tagId, startTime, id: insertResult.insertedId})
}));

app.delete('/timer/:timerId', asyncMiddleware(async (req, res, next) => {
    let db = await mongoClient.connect(DB_URL)
    let userId = await verifyAuthorized(req.headers, db)
    let timerId = ObjectId(req.params.timerId)
    let result = await db.collection('timers').remove({_id: timerId})
    if(result.result.n === 0) throw new ServerError(404, 'Timer not found!')
    db.close()
    res.status(204).send()
}));

app.get('/timers', asyncMiddleware(async (req, res, next) => {
    let db = await mongoClient.connect(DB_URL)
    let userId = await verifyAuthorized(req.headers, db)
    let timers = await db.collection('timers').find({userId}).toArray()
    timers = timers.map((item) => {
        return {
            id: item._id, 
            tagId: item.tagId, 
            startTime: item.startTime, 
            endTime: item.endTime
        }
    })
    db.close()
    res.json(timers)
}));

//FIXME: prevent name duplication
app.post('/tag', asyncMiddleware(async (req, res, next) => {
    let db = await mongoClient.connect(DB_URL)
    let userId = await verifyAuthorized(req.headers, db)
    let name = req.body.name
    let id = ObjectId(req.body.id)
    if(isEmpty(name)) throw new ServerError(400, "Missing `name` field")
    if(isEmpty(id)) throw new ServerError(400, "Missing `id` field")
    let existingTag = await db.collection('tags').findOne({_id: id, userId})
    if(!existingTag) throw new ServerError(404, "Tag not found!")
    let replaceResult = await db.collection('tags').findOneAndReplace({_id: id, userId}, {name, userId, _id: id})
    if(replaceResult.ok !== 1) throw new DatabaseError()
    db.close()
    res.json({name, id})
}));

app.put('/tag', asyncMiddleware(async (req, res, next) => {
    let db = await mongoClient.connect(DB_URL)
    let userId = await verifyAuthorized(req.headers, db)
    let name = req.body.name
    if(isEmpty(name)) throw new ServerError(400, "Missing `name` field")
    let existingTag = await db.collection('tags').findOne({name: name, userId})
    if(existingTag) throw new ServerError(202, "Tag with specified name already exists!")
    let insertResult = await db.collection('tags').insertOne({name: name, userId})
    if(insertResult.insertedCount !== 1) throw new DatabaseError()
    db.close()
    res.json({name, id: insertResult.insertedId})
}));

app.delete('/tag/:tagId', asyncMiddleware(async (req, res, next) => {
    let db = await mongoClient.connect(DB_URL)
    let userId = await verifyAuthorized(req.headers, db)
    let tagId = ObjectId(req.params.tagId)
    let result = await db.collection('tags').remove({_id: tagId})
    if(result.result.n === 0) throw new ServerError(404, 'Tag not found!')
    db.close()
    res.status(204).send()
}));

app.get('/tags', asyncMiddleware(async (req, res, next) => {
    let db = await mongoClient.connect(DB_URL)
    let userId = await verifyAuthorized(req.headers, db)
    let tags = await db.collection('tags').find({userId}).toArray()
    tags = tags.map((item) => {
        return {id: item._id, name: item.name}
    })
    db.close()
    res.json(tags)
}));

app.post('/register', asyncMiddleware(async (req, res, next) => {
    let login = req.body.login
    if(isEmpty(login)) throw new ServerError(400, "Missing `login` field")
    if(isEmpty(req.body.password)) throw new ServerError(400, "Missing `password` field")
    let db = await mongoClient.connect(DB_URL)
    let user = await db.collection('users').findOne({login})
    if(user) throw new ServerError(202, "User already exists")
    let hash = await bcrypt.hash(req.body.password, saltLevel)
    let insertResult = await db.collection('users').insertOne({login, password: hash})
    if(insertResult.insertedCount !== 1) throw new DatabaseError()
    let tokens = await createTokens(insertResult.insertedId)
    await db.collection('tokens').findOneAndReplace({userId: tokens.userId}, tokens, {upsert: true})
    db.close();
    return res.json(tokens)
}));

// Middleware

// Default handler
app.use((error, req, res, next) => {
    res.status(error.code || 500).json({ error: error.message || 'Unhandled server error!'});
});

// Start
app.listen(PORT)
console.log(`Started serving`)