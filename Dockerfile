FROM node:alpine

# Create app dir
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

# Debug
# RUN npm install
# For production use
RUN npm install --only=production

COPY server.js ./

EXPOSE 8102/tcp
# Bundle app
CMD [ "npm",  "start" ]