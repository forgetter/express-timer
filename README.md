## About
Simple backend for react timer app. Used for learning node.js and express.

## Functionalities
* Simple Oauth2 like authorization
* CRUD for tags
* CRUD for timers

## Planned activities
* Tests
* Refactor

## Instalation
1. Create network for app
```docker network create timer-app```
2. Run MongoDB
```docker run --rm --network timer-app -v <Mongo data dir>:/data/db -d --name mongo-db mongo```
3. Build and run this docker image
```docker build -t timer-express:1.0.0 .```
```docker run --rm --network timer-app -d --name timer-express -p 8102:8102 timer-express:1.0.0```

Now API should be accesible (port 8102).
